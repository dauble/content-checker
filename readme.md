# cURL Content Checker #

This is a small script designed to scrub content from a URL, compare it to data saved the day before, then trigger an email if the content has changed.

## Version 1.1 ##

### Requirements ###

* Runs on LAMP setup
* You must run the script daily or set up a cronjob
* [PHP Mailer Library](https://github.com/PHPMailer/PHPMailer) _included_
* [PHP Simple HTML DOM Parser](http://simplehtmldom.sourceforge.net/) _included_

### Setup & Installation ###
* Rename `/classes/class.mailer.sample.php` to `/classes/class.mailer.php` and enter your Gmail credentials.
* Replace anything in `/curl.php` with your own messages

### Contribution guidelines ###

* Please fill out a request on the Issue tracker tool

### Changes ###
* 1.1 - Update email to contain old and new data, clear log file after email is sent, replace with new data
* 1.0 - Initial release

### License ###

Copyright (c) 2017 dauble2k5

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.