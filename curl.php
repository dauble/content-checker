<?php

  // start curl
  require_once('classes/class.logger.php');
  require_once('classes/class.mailer.php');
  require_once('simple_html.php');

  $logger = new Logger();

  $html = file_get_html('url-to-scrub');
  $live_html = $html->find("table", 0); // I'm search for the first table in the DOM. Comment this out to scrub all elements in the DOM.

  // print today's data
  echo "<h1>live page content</h1>";
  echo $live_html;
  echo "<hr>";

  // print log's data
  echo "<h1>log file content</h1>";
  $contents = $logger->read("logs/your-file.log");
  echo $contents;

  echo "<hr>";

  // trim to remove extra spacing
  $live_trim = trim($live_html);
  $legacy_trim = trim($contents);

  // compare results, send email if there's a mismatch
  if($live_trim != $legacy_trim) {
    echo "<h1>mismatch</h1>";

    $msg_html = "Your message";
    $msg_html .= "<br><br>";

    $msg_html .= "<table><tr><td><h1>Old Content</h1>" . $legacy_trim . "</td>";
    $msg_html .= "<td style='width:25px;'>&nbsp;</td>";
    $msg_html .= "<td><h1>New Content</h1>" . $live_trim . "</td></tr></table>";

    $mailer = new Mailer();
    $mailer->send_email_message("Content Mismatch Alert", $msg_html);

    $logger->clear();
    $logger->write($live_html);
  }

?>