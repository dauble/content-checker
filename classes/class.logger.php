<?php
  /*
      Class: Logger

  */
  class Logger{

    //use to log history
    function write($var) {
      $log = "";

      $filename = "your-file";
      $fd = fopen('logs/' . $filename . ".log", "a");

      fputs($fd, $log);

      if (strlen($var) > 0) {
        $log .= " " . str_pad($var, 25);
        fputs($fd, $log);
      }

      fclose($fd);
    }

    function read($file) {
      $handle = fopen($file, 'r') or die('Failed to open file');
      $contents = fread($handle, filesize($file)) or die('failed to read file');

      fclose($handle);

      return $contents;
    }

    function clear() {
      $file = "logs/your-file.log";
      $handle = fopen($file, 'w') or die('Failed to open file');

      fclose($handle);
    }
  }
?>