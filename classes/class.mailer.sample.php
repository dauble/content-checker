<?php
  require_once ('phpmailer/PHPMailerAutoload.php');

  /**
   * Class Mailer
   */
  class Mailer {

    function send_email_message($subject, $msgHTML) {
      $mail = new PHPMailer;
      $mail->isSMTP();
      $mail->SMTPAuth = true;
      $mail->SMTPSecure = "";                 // sets the prefix to the servier
      $mail->Host       = "";                 // sets GMAIL as the SMTP server
      $mail->Port       = ;                   // set the SMTP port for the GMAIL server
      $mail->Username   = "";                 // GMAIL username
      $mail->Password   = "";                 // GMAIL password
      $mail->isHTML(true);

      $mail->setFrom('', '');     // ex: $mail->setFrom('from_mail@noemail.com', 'From Name');
      $mail->addAddress('', '');  // ex: $mail->addAddress('to_email@noemail.com', 'To Name');

      $mail->Subject = $subject;
      $mail->msgHTML($msgHTML);

      if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
      } else {
        echo "Message sent!\n";
      }
    }
  }



?>